import { Button, Container, TextField } from "@mui/material";
import styles from "./contact.module.scss";
import useTranslation from "next-translate/useTranslation";
import { styled } from "@material-ui/styles";
import { useState } from "react";
import { useRouter } from 'next/router';


export function Contact() {
  const { t } = useTranslation("common");
  const [values, setValues] = useState({
    name: "",
    email: "",
    message: "",
    phone: ""
  })

  const origin =
  typeof window !== 'undefined' && window.location.origin
      ? window.location.origin
      : '';

const URL = `${origin}`;



console.log(URL);
  const {name, email, message, phone} = values

  const sendEmail = async (e) => {
    e.preventDefault() 
    try{
      await fetch(`${origin}/api/contact`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(values)
      })
    }catch(err){
      console.log(err)
    }
  }


  
  const CustomTextfield = styled(TextField)({
    "& .MuiInputBase-input": {
      padding: "10px 12px",
    },
  });

  const handleChange = (e) => {
    setValues({...values, [e.target.name]: e.target.value})
  }

  return (
    <>
      <div className={styles.background}>
        <Container>
          <div className={styles.form}>
            <form onSubmit={sendEmail} method="post">
              <div className={styles.header}> {t("contactUs")} </div>
              <div className={styles.formSection}>
                <CustomTextfield
                  placeholder={t("phoneNumber")}
                  className={styles.inputField}
                  value={phone}
                  name="phone"
                  onChange={handleChange}
                />
                <CustomTextfield
                  placeholder={t("companyName")}
                  className={styles.inputField}
                  value={name}
                  name="name"
                  onChange={handleChange}
                />
                <CustomTextfield
                  placeholder={t("email")}
                  className={styles.inputField}
                  value={email}
                  name="email"
                  onChange={handleChange}
                  // onChange={(e) => setEmail(e.target.value)}
                />
                <textarea
                  placeholder={t("message")}
                  value={message}
                  name="message"
                  onChange={handleChange}
                  // onChange={(e) => setMessage(e.target.value)}
                />
                <div className={styles.button}>
                  <Button type="submit"> {t("send")} </Button>
                </div>
              </div>
            </form>
          </div>
        </Container>
      </div>
    </>
  );
}
