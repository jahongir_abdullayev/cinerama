export const homePartners = [
  { content : [
     {
       id: 1,
       title: "Photography",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/lg.png",
     },
     {
       id: 2,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/sony.png",
     },
     {
       id: 3,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/hisense.png",
     },
     {
       id: 4,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/samsung.png",
     },
    
     {
       id: 5,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/gree.png",
     },
     {
       id: 6,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/panasonic.png",
     },
     {
       id: 7,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/skyworth.png",
     },
     {
      id: 8,
      title: "Photography",
      body: "Bootstrap Carousel Example",
      imageUrl: "/images/partners/toshiba.png",
    },
     
   ]},
 
   {content : [
     {
         id: 1,
         title: "Photography",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/aux.png",

       },
       {
         id: 2,
         title: "City Views",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/tcl.png",

       },
       {
         id: 2,
         title: "City Views",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/daewoo.png",

       },
       {
         id: 2,
         title: "City Views",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/changhong.png",

       },
       {
         id: 1,
         title: "Photography",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/xingx.png",

       },
       {
         id: 2,
         title: "City Views",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/vestel.png",

       },
       {
         id: 2,
         title: "City Views",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/ugur.png",

       },
       {
         id: 2,
         title: "City Views",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/homa.png",

       },
   ]}
   
 ];







 export const companyPartners = [
  { content : [
     {
       id: 1,
       title: "Photography",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/hikvision.png",
     },
     {
       id: 2,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/lg.png",
     },
     {
       id: 3,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/samsung.png",
     },
     {
       id: 4,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/deepcool.png",
     },
    
     {
       id: 5,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/acer.png",
     },
     {
       id: 6,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/asus.png",
     },
     {
       id: 7,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/lenovo.png",
     },
     {
      id: 8,
      title: "Photography",
      body: "Bootstrap Carousel Example",
      imageUrl: "/images/partners/kingspec.png",
    },
     
   ]},
 
   {content : [
     {
         id: 1,
         title: "Photography",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/alhua.png",

       },
       {
         id: 2,
         title: "City Views",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/teamgroup.png",

       },
       {
         id: 2,
         title: "City Views",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/huawei.png",

       },
       {
         id: 2,
         title: "City Views",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/gapoo.png",

       },
       {
         id: 1,
         title: "Photography",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/lexar.png",

       },
       {
         id: 2,
         title: "City Views",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/toshiba.png",

       },
       {
         id: 2,
         title: "City Views",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/a4tech.png",

       },
       {
         id: 2,
         title: "City Views",
         body: "Bootstrap Carousel Example",
         imageUrl: "/images/partners/hiwatch.png",

       },
   ]},
   {content: [
    {
      id: 2,
      title: "City Views",
      body: "Bootstrap Carousel Example",
      imageUrl: "/images/partners/teamwolf.png",
    },
    {
      id: 2,
      title: "City Views",
      body: "Bootstrap Carousel Example",
      imageUrl: "/images/partners/netis.png",
    },
    {
      id: 2,
      title: "City Views",
      body: "Bootstrap Carousel Example",
      imageUrl: "/images/partners/lexar.png",
    },
    {
      id: 2,
      title: "City Views",
      body: "Bootstrap Carousel Example",
      imageUrl: "/images/partners/axle.png",
    }
   ]}
   
 ];



 export const ourPartners = [
  { content : [
     {
       id: 1,
       title: "Photography",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/kenwood.png",
     },
     {
       id: 2,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/pioneer.png",
     },
     {
       id: 3,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/magicar.png",
     },
     {
       id: 4,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/kumhotyres.png",
     },
    
     {
       id: 5,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/nama.png",
     },
     {
       id: 6,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/nexen.png",
     },
     {
       id: 7,
       title: "City Views",
       body: "Bootstrap Carousel Example",
       imageUrl: "/images/partners/delkor.png",
     },
     {
      id: 8,
      title: "Photography",
      body: "Bootstrap Carousel Example",
      imageUrl: "/images/partners/immer.png",
    },
     
   ]},
 
   
   
 ];